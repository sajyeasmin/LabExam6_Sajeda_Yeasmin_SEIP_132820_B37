<?php
require_once("../../../vendor/autoload.php");

use App\book_title\BookTitle;
use App\birthday\Birthday;
use App\city\City;
use App\email\Email;
use App\gender\Gender;
use App\hobbies\Hobbies;
use App\profile_picture\Profile_Picture;
use App\summary_of_organization\Summary_Of_Organization;

$book= new BookTitle();
$book->index();

$birth=new Birthday();
$birth->index();

$city=new City();
$city->index();

$email=new Email();
$email->index();

$gender= new Gender();
$gender->index();

$hobbies= new Hobbies();
$hobbies->index();

$picture= new Profile_Picture();
$picture->index();

$summary= new Summary_Of_Organization();
$summary->index();
?>